# A Simple Sample Delay Calculator

Use this Apple Script to calculate the delay (in both milliseconds and samples) between two microphones for phase adjustment.

It asks for:
- Air Temp (C)
- Distance (cm)
- Recording Rate (kHz)

That's it. 

Editable version of script provided for review/improvement.
