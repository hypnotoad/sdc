set temperature to "" as real

tell application "Finder"
	
	display dialog "Enter the ambient air temperature in Celsius (˚C):" default answer temperature
	
	set temperature to text returned of the result
	
end tell

set mic_distance to "" as real

tell application "Finder"
	
	display dialog "Enter the distance between microphones in centimeters:" default answer mic_distance
	
	set mic_distance to text returned of the result
	
end tell

set sample_rate to "" as real

tell application "Finder"
	
	display dialog "Enter your recording sample rate in kHz:" default answer sample_rate
	
	set sample_rate to text returned of the result
	
end tell

-- setting the speed of sound in m/s based on air temperature
set speed_of_sound to (331.3 + (0.606 * temperature))

-- Calulating the number of samples by figuring out how long it takes the sound to travel
-- the distance between the microphone capsules
-- Speed of sounds is modified to cm/s 
-- Time is returned in milliseconds
set samples to ((sample_rate * 1000) / ((speed_of_sound) * 100)) * mic_distance as integer
set time_delay to (samples / (sample_rate * 1000)) * 1000 as integer
tell application "Finder"
	display dialog "The approximate delay between the two microphones is: " & samples & " samples" & return & "or " & (time_delay) & " millisecond(s)."
end tell